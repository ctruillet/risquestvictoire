let dataMassifs = {};
let massifs = []
let dataStVictoire = {};
let dataConcors = {};

let isDataLoaded = false


function preload() {
  dataMassifs = loadJSON("http://bpatp.paca-ate.fr/services/getMassifs.php", loaded);
}

function setup() {
  createCanvas(720, 400);
}

function draw() { 
  if(isDataLoaded){
    drawMassif(massifs)
  }
}

function loaded() {
  massifs.push(dataMassifs.features[22])
  massifs.push(dataMassifs.features[8])
  dataStVictoire = dataMassifs.features[22]
  dataConcors = dataMassifs.features[8]
  console.log(massifs)

  isDataLoaded = true
}

function drawMassif(massif){
  //strokeWeight(0.1);
  noStroke()
  fill(0)
  textSize(38);
  textAlign(LEFT, TOP);
  //textStyle(BOLD);
  text("AUJOURD'HUI",10,12)
  for(let i = 0; i < massifs.length; i++){
    switch(massifs[i].properties.Niveau){
      case 0:
        fill(108,255,0)
        break;
      case 1:
        fill(255,255,0)
        break;
      case 2:
        fill(255,57,57)
        break;
    }
    rect(10,60 + i * 110 ,250,100)
    fill(0)
    textSize(28);
    //textStyle(BOLD);
    textAlign(CENTER, CENTER);
    text(massifs[i].properties.Nom,135,90 + i * 110)
    textSize(20);
    textStyle(ITALIC);
    textAlign(LEFT, CENTER);

    switch(massifs[i].properties.Niveau){
      case 0:
        text("Risque minime",20,130 + i * 110)
        break;
      case 1:
        text("Risque sévère",20,130 + i * 110)
        break;
      case 2:
        text("Risque maximal",20,130 + i * 110)
        break;
    }
    
  }
}
